Slim Framework Foil View
========================

This is a Slim Framework view layer built on top of Foil native template engine. You can use this component to create and render templates in your Slim Framework application. It is based on Slim Framework's [Twig-View](https://github.com/slimphp/Twig-View).

Requires PHP 5.5.0, Slim Framework 3 and Foil 0.6.3 or newer.

## Usage

```php
/**
 * Create new slim application instance.
 */
$app = new \Slim\App();

/**
 * Get the container.
 */
$container = $app->getContainer();

/**
 * Add the foil view helper to the container.
 */
$container['view'] = function ($c)
{
    /**
     * Create new foil view helper instance.
     */
    $view = new \Slim\Views\Foil('path/to/templates');

    /**
     * Load additional extension.
     */
    $view->loadExtension(new \Foil\Extension\Uri(), [
        'pathinfo' => $c['request']->getUri()->getPath()
    ]);

    /**
     * Register foil view helper.
     */
    return $view;
};

/**
 * Example route.
 */
$app->get('/hello/{name}', function ($request, $response, $args)
{
    /**
     * Render the template.
     */
    return $this->view->render($response, 'profile', [
        'name' => $args['name']
    ]);
});

/**
 * Run the application.
 */
$app->run();
```

## License

This open-source software is licensed under the [MIT License](LICENSE).

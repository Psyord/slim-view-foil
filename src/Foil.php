<?php

namespace Slim\Views;

use ArrayAccess;
use Foil\Foil as Environment;
use Psr\Http\Message\ResponseInterface;

class Foil implements ArrayAccess
{

    /**
     * Foil engine instance.
     *
     * @var \Foil\Engine
     */
    protected $engine;

    /**
     * Template data container.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Create new foil view helper instance.
     *
     * @param string|array $path
     * @param array $options
     * @param array $providers
     * @return void
     */
    public function __construct($path, array $options = [], array $providers = [])
    {
        $options['folders'] = is_array($path) ? $path : [$path];

        $this->engine = Environment::boot($options, $providers)->engine();
    }

    /**
     * Non-existent methods are fired by foil engine.
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        return call_user_func_array([$this->engine, $name], $arguments);
    }

    /**
     * Return the rendered template.
     *
     * @param string $template
     * @param array $data
     * @return string
     */
    public function fetch($template, array $data = [])
    {
        $data = array_merge($this->data, $data);

        return $this->engine->render($template, $data);
    }

    /**
     * Bind the rendered template to the response body.
     *
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param string $template
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function render(ResponseInterface $response, $template, array $data = [])
    {
        $response->getBody()->write($this->fetch($template, $data));

        return $response;
    }

    /**
     * Return the foil engine instance.
     *
     * @return \Foil\Engine
     */
    public function getEngine ()
    {
        return $this->engine;
    }

    /**
     * Check if the collection item exists.
     *
     * @param string $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return array_key_exists($key, $this->data);
    }

    /**
     * Return the collection item.
     *
     * @param string $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->data[$key];
    }

    /**
     * Set the collection item.
     *
     * @param string $key
     * @param mixed $value
     */
    public function offsetSet($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Remove item from the collection.
     *
     * @param string $key
     */
    public function offsetUnset($key)
    {
        unset($this->data[$key]);
    }

    /**
     * Return the number of items in collection.
     *
     * @return int
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * Return the collection iterator.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

}
